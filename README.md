# Welcome to my Python / Tornado / MongoDB demo

Please note that this demo is meant for general demonstration and proof-of-concept and is not optimized for production. In a production environment, you'd do a few things differently:

1) The implementation wouldn't be part of the same project as the API.

2) Sensitive key information would be put in a secure keystore and you'd fold that data in during the deploy process using CI/CD.

## Usage

When run locally, you must first provision a MongoDB instance (I used a local Docker MongoDB container) and update the settings/base.py or local.py accordingly.

Next, install the requirements:

```bash
pip install -r requirements.txt
```

Once running, there are two endpoints you can call:

http://localhost:1111/api_demo/ will give you the directory of endpoints (currently there's only one)
http://localhost:1111/api_demo/games/{year}/{week}/ will give you the game information for the year and week specified (ex: http://localhost:1111/api_demo/games/2018/2/). 

If you want to see a formatted display instead of just the JSON data, add a ?render=1 param to the end of the call (ex: http://localhost:1111/api_demo/games/2018/2/?render=1).