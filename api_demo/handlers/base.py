import tornado.web
from api_demo.mixins.handlers import HandlerMixin
from api_demo.utils.utils import to_bool


class BaseHandler(HandlerMixin, tornado.web.RequestHandler):
    def initialize(self):
        super(BaseHandler, self).initialize()
        self._class_name = type(self).__name__
        self._config = self.application._config

        render = to_bool(self.get_argument('render', 0))

        if render:
            self.set_header('Content-Type', 'text/html')
        else:
            self.set_header('Content-Type', 'application/json')
