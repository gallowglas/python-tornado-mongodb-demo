from base import BaseHandler
from api_demo.utils.utils import encode_json


class IndexHandler(BaseHandler):
    _cachedResponse = None

    def _endpoints(self):
        endpoints = {}
        for endpoint in self.settings['endpoints']:
            if not endpoint.handler_class == type(self):
                methods = self._get_methods(endpoint.handler_class)
                for method in methods:
                    key = '{0}_{1}'.format(method.lower(), endpoint.name)
                    id_name = endpoint.handler_class.__module__.split('.')[-1].rstrip('s')

                    path = endpoint.matcher._path

                    if endpoint.name == 'get_scoreboard_data':
                        path = path.replace('%s', '{year}', 1).replace('%s', '{week}', 1)

                    path = path.replace('%s', '{%s_id}' % id_name)
                    endpoints[key] = path
        return endpoints

    def get(self, *args, **kwargs):
        if not IndexHandler._cachedResponse:
            endpoints = self._endpoints()
            IndexHandler._cachedResponse = encode_json({'endpoints': endpoints}, self._config.DEBUG)

        self.write(IndexHandler._cachedResponse)