from base import BaseHandler
from api_demo.utils.utils import encode_json, to_bool, flatten_dict
from api_demo.settings import NCAA_API_BASE, NCAA_SCOREBOARD_API
import requests, json
from datetime import datetime, timedelta
from time import strftime, localtime


class ScoreboardHandler(BaseHandler):
    def initialize(self, games_data):
        super(ScoreboardHandler, self).initialize()
        self._games_data = games_data

    @staticmethod
    def _is_live(games):
        for game in games.get('scoreboard', []):
            if game.get('currentPeriod', 'Final') != 'Final':
                return True

        return False

    def _get_scoreboard_data(self, year, week):
        week_str = '0{}'.format(week)[-2:]
        url = NCAA_SCOREBOARD_API.format(year, week_str)
        data = requests.get(url)

        if data.status_code != 200:
            raise Exception

        data = json.loads(data.content)
        data = data['scoreboard']

        for gameday in data:
            games = gameday.get('games', [])
            gameday['game_data'] = []

            for game_path in games:
                game_details = self._get_game_details(game_path)
                gameday['game_data'].append(game_details)

            gameday.pop('games')

        return data

    @staticmethod
    def _get_game_details(game_path):
        url = '{}{}'.format(NCAA_API_BASE, game_path)
        data = requests.get(url)

        if data.status_code != 200:
            raise Exception

        data = json.loads(data.content)

        local_time = strftime('%I:%M %p', localtime(float(data['startTimeEpoch'])))

        data.update({'local_time': local_time})

        return data

    def _fetch_data(self, year, week, now, cached):
        scoreboard_data = self._get_scoreboard_data(year, week)

        games = {
            '_id': {
                'year': year,
                'week': week
            },
            'scoreboard': scoreboard_data,
            'fetch_date': now
        }

        live_flag = self._is_live(games)  # since this is fetched data, we have to check again here

        # TODO: This should be an upsert
        try:
            if not cached:
                self._games_data.insert(games)
            else:
                game_id = games.pop('_id')
                for k, v in list(game_id.items()):
                    game_id['_id.' + k] = game_id.pop(k)
                self._games_data.update(game_id, games)
        except Exception as e:
            pass  # if we can't cache it, move on

        return live_flag, games

    def get(self, year, week, *args, **kwargs):
        try:
            week = int(week)
            year = int(year)
        except Exception as e:
            self.write_error(400, response_message='Parameter value error',
                             log_message='Parameter value error',
                             exc_info=e, extra=self.extra)
            return

        try:
            games = self._games_data.find_one({'_id.year': year, '_id.week': week})
        except Exception as e:
            games = None

        now = datetime.now()

        if games:
            cached = True
            then = games.get('fetch_date', now - timedelta(minutes=15))
            live_flag = self._is_live(games)
        else:
            cached = False
            then = now - timedelta(minutes=15)
            live_flag = None  # might be live or might not be

        cache_age_secs = (now-then).seconds

        # if all the games have expired, there's no reason to refresh the cache
        refresh_cache = cache_age_secs > 60 and live_flag

        if not games or (live_flag and refresh_cache):
            try:
                live_flag, games = self._fetch_data(
                    week=week,
                    year=year,
                    now=now,
                    cached=cached
                )
            except Exception as e:
                live_flag = False
                games = []

        render = to_bool(self.get_argument('render', 0))

        if render:
            self.render(
                "../templates/scoreboard.html",
                title="Games for Week {}".format(week),
                data=games,
                cached=not refresh_cache,
                cache_age=cache_age_secs,
                live_flag=live_flag
            )
        else:
            self.write(encode_json(obj=games, pretty_print=True))

