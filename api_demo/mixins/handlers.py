"""Handy mixins for handlers

handlers.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:copyright: (c) 2015 by iCitizen.
"""
import os
import json

from copy import deepcopy

from api_demo.utils.utils import encode_json
from httplib import responses

ENVIRONMENT_LOG_TOKEN = 'LOG_TOKEN' in os.environ
ENVIRONMENT_LOG_REQUEST_BODY = 'LOG_REQUEST_BODY' in os.environ


class HandlerMixin(object):
    def initialize(self):

        super(HandlerMixin, self).initialize()

        self.extra = {
            'handler': self.__class__.__name__,
            'uri': self.request.uri,
            'client_version': self.client_version,
            'user_agent': self.user_agent,
            'http_method': self.request.method
        }

    @property
    def service_name(self):
        return self._config.SERVICE_NAME

    @property
    def client_version(self):
        return float(self.request.headers.get('client-version', 2.0))

    @property
    def user_agent(self):
        return self.request.headers.get('User-Agent')

    def get_user_locale(self):
        pass

    def prepare(self):

        if ENVIRONMENT_LOG_TOKEN or self._config.DEBUG:
            x_auth_token = self.request.headers.get('x-auth-token', None)
            if x_auth_token:
                self.extra.update({'x-auth-token': x_auth_token})

        if ENVIRONMENT_LOG_REQUEST_BODY:
            if self.request.body:
                self.extra.update({'request.body': repr(json.loads(self.request.body))})

    def on_finish(self):
        self.cleanup()

    def on_close(self):
        self.cleanup()

    def cleanup(self):
        pass

    def options(self, *args, **kwargs):
        """Restful OPTIONS method for querying a list of supported Restful
        methods for a handler.

        Args:
            args (list): Unnamed argument values
            kwargs (dict): Named argument values
        """
        methods = self._get_methods(type(self))
        methods.append('OPTIONS')
        self.set_header('Access-Control-Allow-Methods', ",".join(methods))
        self.set_status(204)
        self.finish()

    def _get_methods(self, handler):
        """Internal method for returning the Restful methods available for a
        particular handler.

        Args:
            handler (tornado.web.RequestHandler): The request handler

        Returns:
            list: The methods available for the specified request handler
        """
        return [key.upper() for key in handler.__dict__ if key.upper() in handler.SUPPORTED_METHODS]

    def write_error(self, status_code, response_message=None, log_message=None, reason=None, exc_info=None,
                    extra={},
                    response_errors=None):

        """Base handler for logging errors and displaying error messages to the
        client. Completes by writing the error to the response.

        Args:
            response_errors (dict): A dictionary of errors with field names.
            status_code (int): Response status code value
            response_message (str): Message included in response to client
            log_message (str): Message logged internally (not viewable to client)
            reason (str): Reason for error (usually included by HTTPError)
            exc_info (Exception): An exception object, if applicable
            extra (dict): Any extra information to be included in log messages

        """
        # clean up exc_info if necessary
        # - Built-in Tornado exceptions pass in a tuple
        # - SQLAlchemy exceptions pass in an Exception
        # - Caught exceptions pass in an Exception
        if hasattr(exc_info, '__iter__'):  # iterable, find the Exception
            exc_info = next(e for e in exc_info if isinstance(e, Exception))

        response_errors = None

        status_code, response_message, log_message, reason, exc_info, \
        extra, response_errors = self._apply_defaults(status_code, response_message, log_message, reason, exc_info,
                                                      extra, response_errors)

        error_include_in_response = self._config.ERROR_INCLUDE_IN_RESPONSE or self._config.DEBUG

        # response message should always override anything else to the client
        client_message = response_message
        exc_message = None
        reason_message = reason or responses.get(status_code, None)

        if exc_info is not None:
            exc_message = getattr(exc_info, 'message', None)

        error_log_message = log_message or response_message or exc_message or reason_message or 'Unknown Error'

        if client_message is None:
            client_message = 'API Exception'

        # Only expose error details if we are in debug
        if error_include_in_response:
            client_message = '{} [{}]'.format(client_message, error_log_message)

        if reason is not None:
            self.set_status(status_code, reason=reason)
        else:
            self.set_status(status_code)

        if exc_info is None:
            extra.update({'status_code': status_code, 'reason': reason})

        self.set_header('Content-Type', 'application/json')
        self.write(
            encode_json(
                {
                    'error': client_message,
                    'errors': response_errors,
                    'status_code': status_code,
                    'reason': reason
                },
                self._config.DEBUG
            )
        )

    def _apply_defaults(self, status_code, response_message, log_message, reason, exc_info,
                        extra, response_errors):

        return status_code, response_message, log_message, reason, exc_info, \
               extra, response_errors

    def _add_debug_logging_details(self, response_errors, extra):

        log_extra = deepcopy(self.extra)

        log_extra.update(extra)

        if response_errors:
            log_extra.update({'response_errors': response_errors})

        return log_extra
