import os

ENVIRONMENT = os.environ.setdefault("ENV", "local")

from base import *

__mod_dir = os.path.dirname(__file__)
__mod_name = '{}.py'.format(ENVIRONMENT)
__mod_file = '{}/{}'.format(__mod_dir, __mod_name)
__namespace = locals()
if os.path.isfile(__mod_file):
    try:
        execfile(__mod_file, __namespace)
    except Exception as e:
        raise Exception("Failed to load settings from: '{}'.".format(__mod_file), e)
    globals().update(__namespace)