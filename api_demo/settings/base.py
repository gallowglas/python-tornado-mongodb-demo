import os
from api_demo.settings import ENVIRONMENT

ENV = ENVIRONMENT
SERVICE_NAME = 'api_demo'
API_VERSION = '2.0.0'
MAJOR_VERSION = API_VERSION.split('.')[0]
JSONIFY_PRETTYPRINT_REGULAR = True
DEBUG = False
TESTING = False

MONGO_CLIENT = {
    'DB_URI': os.environ.get('MONGO_DB_URI', 'mongodb://192.168.99.100:32772'),
    'MAX_POOL_SIZE': os.environ.get('MONGO_MAX_POOL_SIZE', 5)
}

NCAA_API_BASE = 'http://data.ncaa.com/'
NCAA_SCOREBOARD_API = 'http://data.ncaa.com/sites/default/files/data/scoreboard/football/fbs/{}/{}/scoreboard.json'
