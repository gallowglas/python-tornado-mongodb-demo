from json import loads, dumps, JSONEncoder
from uuid import UUID
from datetime import datetime, date, timedelta


def flatten_dict(d):
    """Method for flattening dictionary keys into MongoDB like dot notation.

    Args:
        d (dict): Nested python dictionary structure

    Returns:
        (dict) A flattened dictionary structure with dot notation keys
    """
    def items():
        for key, value in d.items():
            if isinstance(value, dict):
                for subkey, subvalue in flatten_dict(value).items():
                    yield key + "." + subkey, subvalue
            else:
                yield key, value

    return dict(items())


class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            return str(obj)
        elif isinstance(obj, datetime):
            if obj.utcoffset() is not None:
                obj = obj - obj.utcoffset()
            return obj.isoformat()
        elif isinstance(obj, date):
            return obj.isoformat()
        elif isinstance(obj, timedelta):
            return obj.seconds
        else:
            return JSONEncoder.default(self, obj)

    def encode(self, obj):
        return JSONEncoder.encode(self, obj)

    def iterencode(self, o, _one_shot=False):
        return JSONEncoder.iterencode(self, o, _one_shot=False)


class CustomJSON(object):
    @staticmethod
    def loads(obj):
        return loads(obj)

    @staticmethod
    def dumps(obj):
        return dumps(obj, cls=CustomJSONEncoder)


def encode_json(obj, pretty_print=True, cls=CustomJSONEncoder):
    if pretty_print:
        val = dumps(obj=obj, cls=cls, indent=4, sort_keys=True)
    else:
        val = dumps(obj=obj, cls=cls)

    return val


def to_bool(value):
    valid = {'true': True, 't': True, '1': True,
             'false': False, 'f': False, '0': False,
             }

    if isinstance(value, bool):
        return value

    if isinstance(value, int):
        return bool(value)

    if not isinstance(value, basestring):
        raise ValueError('invalid literal for boolean. Not a string.')

    lower_value = value.lower()
    if lower_value in valid:
        return valid[lower_value]
    else:
        raise ValueError('invalid literal for boolean: "%s"' % value)
