import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.log
import tornado.web
from tornado.options import define, options

from pymongo import MongoClient

from api_demo.handlers import index, main

from config import Config

# options
define("port", default=1111, help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self, config, games_data):
        """ Class constructor
        :param config_obj:
            Module and Class Name to use for providing configuration settings
        """
        self._config = config
        self._games_data = games_data

        # handlers - order matters here use most specific first
        base_pattern = '/{}/'.format(self._config.SERVICE_NAME)

        request_handlers = [
            tornado.web.url(
                pattern='{}'.format(base_pattern),
                handler=index.IndexHandler,
                name='index'
            )
        ]

        request_handlers.append(
            tornado.web.url(
                pattern='{}games/(.+?)/(.+?)/'.format(base_pattern),
                handler=main.ScoreboardHandler,
                name='get_scoreboard_data',
                kwargs={
                    'games_data': self._games_data
                }
            )
        )

        # app
        settings = dict(debug=self._config.DEBUG, endpoints=request_handlers)
        super(Application, self).__init__(request_handlers, **settings)


if __name__ == "__main__":
    tornado.options.parse_command_line()

    # populate config settings
    config = Config()
    config.from_module('api_demo.settings')

    mongo_client = MongoClient(
        config.MONGO_CLIENT['DB_URI'],
        maxPoolSize=config.MONGO_CLIENT['MAX_POOL_SIZE'],
        serverSelectionTimeoutMS=1
    )

    games_data = mongo_client['sports_data']['games']

    # create application object
    app = Application(config, games_data=games_data)
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)

    # Display host address in the console
    if app.default_host:
        print 'http://{}:{}/{}/'.format(app.default_host, options.port, config.SERVICE_NAME)
    else:
        print 'http://localhost:{}/{}/'.format(options.port, config.SERVICE_NAME)

    # start io loop
    tornado.ioloop.IOLoop.instance().start()
