from importlib import import_module


class Config(dict):
    """Class that represents configuration options."""

    def __init__(self, defaults=None):
        """Class constructor.
        Args:
            defaults: Configuration defaults (default: None)
        """
        dict.__init__(self, defaults or {})

    def from_module(self, module_name):
        """Populates configuration values using a dictionary object.
        Args:
            module_name: Full path module name to use for providing configuration settings
        """
        module_obj = import_module(module_name)
        [setattr(self, key, getattr(module_obj, key)) for key in dir(module_obj) if key.isupper()]

    def __getattr__(self, key):
        """Overridden Method for Accessing dictionary values as if they were class attributes.
        Args:
            key: Dictionary key to use when retrieving property value
        Returns:
            Dictionary value based on the key argument provided
        """
        if key in self:
            return self[key]
        return None

    def __setattr__(self, key, value):
        """Overridden Method for Setting a dictionary key's value as if it was a class attribute.
        Args:
            key: Dictionary key for the value specified
            value: Dictionary value to be set for the specified key
        """
        self[key] = value
